package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Temporada {
    private ArrayList<Episodio> Episodios;

    public ArrayList<Episodio> getEpisodios() {
        return Episodios;
    }

    public void setEpisodios(ArrayList<Episodio> episodios) {
        Episodios = episodios;
    }
}
