package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Serie extends Produccion {
    private String Inicio;
    private String Fin;
    private ArrayList<Temporada> Temporadas;

    public Serie(String inicio, String fin, ArrayList<Temporada> temporadas, String productor, ArrayList<Actor> actores, Float calificacion, String director) {
        this.Inicio = inicio;
        this.Fin = fin;
        this.Temporadas = temporadas;
        super.Productor = productor;
        super.Actores = actores;
        super.Calificacion = calificacion;
        super.Director = director;
    }
}
