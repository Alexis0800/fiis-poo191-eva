package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Personaje personaje= new Personaje();
        personaje.setCapitulo("2");
        personaje.setVeces(2);
        Actor actor= new Actor();
        actor.setNombre("Juan");
        ArrayList<Personaje> aux1 = new ArrayList<Personaje>();
        aux1.add(personaje);
        actor.setPersonajes(aux1);
        ArrayList<Actor> aux2 = new ArrayList<Actor>();
        aux2.add(actor);

        Pelicula pelicula = new Pelicula("Hola", "Peru", "Español","12/05/2010", "Juan Castro", aux2, 4f, "Juan");

        Documental documental =new Documental("Hola", "Peru", "Español","12/05/2010", "Juan Castro", aux2, 4f, "Juan");

        Episodio episodio = new Episodio();
        episodio.setCalificacion(3f);
        episodio.setPersonajes(aux1);
        episodio.setUrl("www.peruflix.com/hola/episodio1");
        ArrayList<Episodio> aux3 = new ArrayList<Episodio>();
        aux3.add(episodio);
        Episodio episodio2 = new Episodio();
        episodio2.setCalificacion(2f);
        episodio2.setUrl("www.peruflix.com/hola/episodio2");
        episodio2.setPersonajes(aux1);
        aux3.add(episodio2);
        Temporada temporada = new Temporada();
        temporada.setEpisodios(aux3);
        ArrayList<Temporada> aux4 = new ArrayList<Temporada>();
        aux4.add(temporada);
        Serie serie = new Serie("12/2/2010", "20/2/2018", aux4,"Juan", aux2, 5f,"Elian");
    }

}
