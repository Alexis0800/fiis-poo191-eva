package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Actor {
    private ArrayList<Personaje> Personajes;
    private String Nombre;

    public ArrayList<Personaje> getPersonajes() {
        return Personajes;
    }

    public void setPersonajes(ArrayList<Personaje> personajes) {
        Personajes = personajes;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
