package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Episodio {
    private String Url;
    private Float Calificacion;
    private ArrayList<Personaje> Personajes;

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public Float getCalificacion() {
        return Calificacion;
    }

    public void setCalificacion(Float calificacion) {
        Calificacion = calificacion;
    }

    public ArrayList<Personaje> getPersonajes() {
        return Personajes;
    }

    public void setPersonajes(ArrayList<Personaje> personajes) {
        Personajes = personajes;
    }
}
