package edu.fiis.uni.pc2;

public class Personaje {
    private String Capitulo;
    private Integer Veces;

    public String getCapitulo() {
        return Capitulo;
    }

    public void setCapitulo(String capitulo) {
        Capitulo = capitulo;
    }

    public Integer getVeces() {
        return Veces;
    }

    public void setVeces(Integer veces) {
        Veces = veces;
    }
}
