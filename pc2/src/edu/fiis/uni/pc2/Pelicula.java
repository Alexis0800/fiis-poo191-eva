package edu.fiis.uni.pc2;

import java.util.ArrayList;

public class Pelicula extends Produccion {
    private String Titulo;
    private String Pais;
    private String Idioma;
    private String Fecha;

    public Pelicula(String titulo, String pais, String idioma, String fecha, String productor, ArrayList<Actor> actores, Float calificacion, String director) {
        this.Titulo = titulo;
        this.Pais = pais;
        this.Idioma = idioma;
        this.Fecha = fecha;
        super.Productor = productor;
        super.Actores = actores;
        super.Calificacion = calificacion;
        super.Director = director;
    }

}
