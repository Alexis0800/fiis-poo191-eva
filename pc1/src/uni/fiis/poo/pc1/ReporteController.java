package uni.fiis.poo.pc1;

public class ReporteController {
    public static void main(String[] args) {
        Aerolinea aerolinea = new Aerolinea();
        aerolinea.setCorreo("lan@gmail.com");
        aerolinea.setDireccion("Av...");
        aerolinea.setPagina("lan.com");

        Aeronave aeronave = new Aeronave();
        aeronave.setCapacidad(50);
        aeronave.setFabricante("Aviones");
        aeronave.setModelo("v1");

        Cliente cliente = new Cliente();
        cliente.setAlergias("maiz");
        cliente.setCorreo("micorreo@gmail.com");
        cliente.setDNI("12345678");
        cliente.setNombre("Alexis");
        cliente.setTelefono("987654321");

        Reserva reserva = new Reserva();
        reserva.setCantidadAsientos(1);
        reserva.setNumeroAsientos(12);
        reserva.setNumeroVuelos(5);

        Vuelo vuelo = new Vuelo();
        vuelo.setAerolinea("Lan");
        vuelo.setCantidadPasajeros(50);
        vuelo.setDuracion(2);
        vuelo.setNumeroAsientos(150);
        vuelo.setNumeroVuelo(132);

        System.out.println(cliente.getNombre());

    }
}
