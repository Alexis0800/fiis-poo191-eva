package uni.fiis.poo.pc1;

public class Reserva {
    private Integer CantidadAsientos;
    private Integer NumeroVuelos;
    private Integer NumeroAsientos;

    public Integer getCantidadAsientos() {
        return CantidadAsientos;
    }

    public void setCantidadAsientos(Integer cantidadAsientos) {
        CantidadAsientos = cantidadAsientos;
    }

    public Integer getNumeroVuelos() {
        return NumeroVuelos;
    }

    public void setNumeroVuelos(Integer numeroVuelos) {
        NumeroVuelos = numeroVuelos;
    }

    public Integer getNumeroAsientos() {
        return NumeroAsientos;
    }

    public void setNumeroAsientos(Integer numeroAsientos) {
        NumeroAsientos = numeroAsientos;
    }
}
