package uni.fiis.poo.pc1;

public class Vuelo {
    private String Aerolinea;
    private Integer NumeroVuelo;
    private Integer Duracion;
    private Integer CantidadPasajeros;
    private Integer NumeroAsientos;

    public String getAerolinea() {
        return Aerolinea;
    }

    public void setAerolinea(String aerolinea) {
        Aerolinea = aerolinea;
    }

    public Integer getNumeroVuelo() {
        return NumeroVuelo;
    }

    public void setNumeroVuelo(Integer numeroVuelo) {
        NumeroVuelo = numeroVuelo;
    }

    public Integer getDuracion() {
        return Duracion;
    }

    public void setDuracion(Integer duracion) {
        Duracion = duracion;
    }

    public Integer getCantidadPasajeros() {
        return CantidadPasajeros;
    }

    public void setCantidadPasajeros(Integer cantidadPasajeros) {
        CantidadPasajeros = cantidadPasajeros;
    }

    public Integer getNumeroAsientos() {
        return NumeroAsientos;
    }

    public void setNumeroAsientos(Integer numeroAsientos) {
        NumeroAsientos = numeroAsientos;
    }
}
