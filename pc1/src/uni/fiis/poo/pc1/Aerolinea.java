package uni.fiis.poo.pc1;

public class Aerolinea {
    private String Pagina;
    private String Direccion;
    private String Correo;

    public String getPagina() {
        return Pagina;
    }

    public void setPagina(String pagina) {
        Pagina = pagina;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }
}
